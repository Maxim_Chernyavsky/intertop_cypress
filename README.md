## Setup:

### Ubuntu

1. Install Node.js.                     (https://nodejs.org/)
2. cd path/to/intertop_cypress/         (in console)
3. npm install                          (in console)
4. ./node_modules/.bin/cypress open     (in console)

### Windows

1. Install Node.js.                     (https://nodejs.org/)
2. cd path\to\intertop_cypress\         (in console)
3. npm install                          (in console)
4. node_modules\.bin\cypress open       (in console)