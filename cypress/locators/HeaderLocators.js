class HeaderLocators {

    AUTH_ICON = '[id="auth_block"][class="user-menu-item personal-menu-item"]'
    LANGUAGE = '[id="launguage-select"] [class="header-select-city comp-select-lang"] [class="city-name"]'
    LANGUAGE_IN_DROPDOWN = '[id="launguage-select"] [class="select-city-top lang-select dropdown_opened"] a'
    MY_PROFILE_TEXT = '[class="bl-reg-log user-log"]:not([style="display: none;"]) a'

}

module.exports = new HeaderLocators()