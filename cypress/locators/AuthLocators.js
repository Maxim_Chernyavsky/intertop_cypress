class AuthLocators {

    EMAIL_INPUT = '[id="email"]'
    PASSWORD_INPUT = '[id="password"]'
    LOGIN_BTN = '[class="user-menu-item personal-menu-item opened"] [class="action-button lg-btn green-btn font-semibold' +
        ' button styl-material login-action"] [role="button"]'
    REGISTRATION_BTN = '[class="user-menu-item personal-menu-item opened"]' +
        ' [class="action-button lg-btn green-btn font-semibold button styl-material register-action"] [role="button"]'

    EMAIL_ALERT_MSG = '//input[@id="email"]/ancestor::div[@role="group"]//div[@role="alert"]'
    PASSWORD_ALERT_MSG = '//input[@id="password"]/ancestor::div[@role="group"]//div[@role="alert"]'
    PHONE_INPUT = '[id="phone"]'
    I_WANT_BONUS_CARD_CHECKBOX = '[id*="_i_want_bonus_card"][type="checkbox"]'
    I_AGREE_CHECKBOX = '[id*="_i_agree"]'
    GENERATE_PASSWORD_FOR_MY_CHECKBOX = '[id*="_generate_pass"]'
    I_AGREE_ALERT_MSG = '[class="agreement_error invalid-feedback"][style=\'\']';
    PHONE_ALERT_MSG = '//input[@id="phone"]/ancestor::div[@role="group"]//div[@role="alert"]'
}

module.exports = new AuthLocators()