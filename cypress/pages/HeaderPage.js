import headerLocators from '../locators/HeaderLocators'

class HeaderPage {


    selectLanguageInDropdown(content) {
        const lang = cy.get(headerLocators.LANGUAGE_IN_DROPDOWN).contains(content)
        lang.click()
    }

    clickLanguage() {
        const lang = cy.get(headerLocators.LANGUAGE)
        lang.click()
    }

    clickAuthIcon() {
        const icon = cy.get(headerLocators.AUTH_ICON, {timeout: 10000}).should('be.visible')
        icon.click()
    }

    getMyProfileText() {
        return cy.get(headerLocators.MY_PROFILE_TEXT).first()
    }
}

module.exports = new HeaderPage()