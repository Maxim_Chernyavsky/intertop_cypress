import authLocators from '../locators/AuthLocators'

class AuthPage {

    typeEmail(email) {
        const input = cy.get(authLocators.EMAIL_INPUT, {timeout: 10000}).should('be.visible')
        input.clear()
        input.type(email)
    }

    typePassword(password) {
        const input = cy.get(authLocators.PASSWORD_INPUT, {timeout: 10000}).should('be.visible')
        input.clear()
        input.type(password)
    }

    clickLoginBtn() {
        const btn = cy.get(authLocators.LOGIN_BTN)
        btn.click()
    }

    getEmailAlertMsgText() {
        return cy.xpath(authLocators.EMAIL_ALERT_MSG)
    }

    getPasswordAlertMsgText() {
        return cy.xpath(authLocators.PASSWORD_ALERT_MSG)
    }

    clickRegistrationBtn() {
        const btn = cy.get(authLocators.REGISTRATION_BTN)
        btn.click()
    }

    typePhone(phone) {
        const input = cy.get(authLocators.PHONE_INPUT)
        input.clear()
        input.type(phone)
    }

    checkBonusCardCheckbox(check) {
        const checkbox = cy.get(authLocators.I_WANT_BONUS_CARD_CHECKBOX)

        if (check === false) {
            checkbox.uncheck({force: true})
        } else {
            checkbox.check({force: true})
        }
    }

    checkIAgreeCheckbox(check) {
        const checkbox = cy.get(authLocators.I_AGREE_CHECKBOX)

        if (check === false) {
            checkbox.uncheck({force: true})
        } else {
            checkbox.check({force: true})
        }
    }

    checkGeneratePasswordCheckbox(check) {
        const checkbox = cy.get(authLocators.GENERATE_PASSWORD_FOR_MY_CHECKBOX)

        if (check === false) {
            checkbox.uncheck({force: true})
        } else {
            checkbox.check({force: true})
        }
    }

    getAgreeWithTermsAlertMsgText() {
        return cy.get(authLocators.I_AGREE_ALERT_MSG)
    }

    getPhoneAlertMsgText() {
        return cy.xpath(authLocators.PHONE_ALERT_MSG)
    }

}

module.exports = new AuthPage()