Feature: Authorization

  User tries to log in

  Background:
    Given I open "https://intertop.ua/" page
    And I change language on "Укр"
    When I click on auth icon

  @focus
  Scenario: With correct credential
    When I type "authorization@mailforspam.com" in "email field"
    And I type "qWERTY1234@" in "password field"
    And I click on "enter" button
    And I click on auth icon
    Then I see "Мій профіль" text in dropdown

  @focus
  Scenario: Password characters of different case
    When I type "authorization@mailforspam.com" in "email field"
    And I type "qwerty1234@" in "password field"
    And I click on "enter" button
    Then I see "Невірний логін або пароль" text in email field
    And I see "Невірний логін або пароль" text in password field

  @focus
  Scenario: Password with space inside
    When I type "authorization@mailforspam.com" in "email field"
    And I type "qWERT Y1234@" in "password field"
    And I click on "enter" button
    Then I see "Невірний логін або пароль" text in email field
    And I see "Невірний логін або пароль" text in password field

  @focus
  Scenario: Password with space before and after
    When I type "authorization@mailforspam.com" in "email field"
    And I type " qWERTY1234@ " in "password field"
    And I click on "enter" button
    Then I see "Пароль мін 10 символів, 1 спец. символ, 1 цифра, буква в верхньому і нижньому регістрі" text in password field

  @focus
  Scenario: Password empty
    When I type "authorization@mailforspam.com" in "email field"
    And I click on "enter" button
    Then I see "Невірний логін або пароль" text in email field
    And I see "Невірний логін або пароль" text in password field

  @focus
  Scenario: Email empty
    When I type "qWERTY1234@" in "password field"
    And I click on "enter" button
    Then I see "Невірний логін або пароль" text in email field
    And I see "Невірний логін або пароль" text in password field

  @focus
  Scenario: Email with space inside
    When I type "authorizat ion@mailforspam.com" in "email field"
    And I type "qWERTY1234@" in "password field"
    And I click on "enter" button
    Then I see "Невірний логін або пароль" text in email field
    And I see "Невірний логін або пароль" text in password field

  @focus
  Scenario: All fields empty
    When I click on "enter" button
    Then I see "Невірний логін або пароль" text in email field
    And I see "Невірний логін або пароль" text in password field