import utilsSteps from "../../steps/utilsSteps"
import headerSteps from "../../steps/headerSteps"
import authSteps from "../../steps/authSteps"

describe('Authorization', () => {

    // Steps
    utilsSteps.openUrl()
    headerSteps.selectLanguage()
    headerSteps.clickAuthIcon()
    authSteps.logIn()
    // Expects
    headerSteps.expectSeeText()
    authSteps.expectAlertTextInEmailField()
    authSteps.expectAlertTextInPasswordField()

})