Feature: Registration

  User is trying to register

  Background:
    Given I open "https://intertop.ua/" page
    And I change language on "Укр"
    When I click on auth icon
    And I click on "Registration" button

  @focus
  Scenario: With correct credential
    When I type an unregistered email
    And I type an unregistered phone number
    And I type "qWERTY1234@" in "password field"
    And I uncheck "I want to get an INTERTOP PLUS card" checkbox
    And I check "I agree with terms" checkbox
    And I click on "Registration" button
    And I click on auth icon
    Then I see "Мій профіль" text in dropdown

  @focus
  Scenario: With automatically generated password
    When I type an unregistered email
    And I type an unregistered phone number
    And I check "Generate a password for me" checkbox
    And I uncheck "I want to get an INTERTOP PLUS card" checkbox
    And I check "I agree with terms" checkbox
    And I click on "Registration" button
    And I click on auth icon
    Then I see "Мій профіль" text in dropdown

  @focus
  Scenario: With empty email field
    When I type an unregistered phone number
    And I type "qWERTY1234@" in "password field"
    And I uncheck "I want to get an INTERTOP PLUS card" checkbox
    And I check "I agree with terms" checkbox
    And I click on "Registration" button
    Then I see "E-mail не заповнений" text in email field

  @focus
  Scenario: Email without box name
    When I type "@mailforspam.com" in "email field"
    And I type an unregistered phone number
    And I type "qWERTY1234@" in "password field"
    And I uncheck "I want to get an INTERTOP PLUS card" checkbox
    And I check "I agree with terms" checkbox
    And I click on "Registration" button
    Then I see "E-mail вказано невірно" text in email field

  @focus
  Scenario: Email with cyrillic in the name
    When I type "регистрация@mailforspam.com" in "email field"
    And I type an unregistered phone number
    And I type "qWERTY1234@" in "password field"
    And I uncheck "I want to get an INTERTOP PLUS card" checkbox
    And I check "I agree with terms" checkbox
    And I click on "Registration" button
    Then I see "E-mail вказано невірно" text in email field

  @focus
  Scenario: With unchecked "I agree with terms" checkbox
    When I type an unregistered email
    And I type an unregistered phone number
    And I type "qWERTY1234@" in "password field"
    And I uncheck "I want to get an INTERTOP PLUS card" checkbox
    And I uncheck "I agree with terms" checkbox
    And I click on "Registration" button
    Then I see "Згода з обробкою даних є обов'язковою" text in "I agree with terms" field

  @focus
  Scenario: All fields empty
    When I uncheck "I want to get an INTERTOP PLUS card" checkbox
    And I check "I agree with terms" checkbox
    And I click on "Registration" button
    Then I see "E-mail не заповнений" text in email field
    And I see "Телефон не заповнений" text in phone field
    And I see "Пароль мін 10 символів, 1 спец. символ, 1 цифра, буква в верхньому і нижньому регістрі" text in password field

  @focus
  Scenario: Email with 320 symbols
    When I type email with "320" symbols in "email" field
    And I type an unregistered phone number
    And I type "qWERTY1234@" in "password field"
    And I uncheck "I want to get an INTERTOP PLUS card" checkbox
    And I check "I agree with terms" checkbox
    And I click on "Registration" button
    Then I see "E-mail вказано невірно" text in email field

  @focus
  Scenario: Email with a space instead of the mailbox name
    When I type "    @mailforspam.com" in "email field"
    And I type an unregistered phone number
    And I type "qWERTY1234@" in "password field"
    And I uncheck "I want to get an INTERTOP PLUS card" checkbox
    And I check "I agree with terms" checkbox
    And I click on "Registration" button
    Then I see "Адреса ел. пошти повинна містити символ \"@\". Введіть частину адреси після символу \"@\". Адреса не повинна містити символи \"\", \":\" і \"/\"." text in email field

  @focus
  Scenario: Email without @mailforspam.com
    When I type "registration" in "email field"
    And I type an unregistered phone number
    And I type "qWERTY1234@" in "password field"
    And I uncheck "I want to get an INTERTOP PLUS card" checkbox
    And I check "I agree with terms" checkbox
    And I click on "Registration" button
    Then I see "E-mail вказано невірно" text in email field

  @focus
  Scenario: Email without dot
    When I type "registration@mailforspamcom" in "email field"
    And I type an unregistered phone number
    And I type "qWERTY1234@" in "password field"
    And I uncheck "I want to get an INTERTOP PLUS card" checkbox
    And I check "I agree with terms" checkbox
    And I click on "Registration" button
    Then I see "E-mail вказано невірно" text in email field

  @focus
  Scenario: Email without @
    When I type "registrationmailforspam.com" in "email field"
    And I type an unregistered phone number
    And I type "qWERTY1234@" in "password field"
    And I uncheck "I want to get an INTERTOP PLUS card" checkbox
    And I check "I agree with terms" checkbox
    And I click on "Registration" button
    Then I see "E-mail вказано невірно" text in email field

  @focus
  Scenario: Email with coma
    When I type "registration@mailforspam,com" in "email field"
    And I type an unregistered phone number
    And I type "qWERTY1234@" in "password field"
    And I uncheck "I want to get an INTERTOP PLUS card" checkbox
    And I check "I agree with terms" checkbox
    And I click on "Registration" button
    Then I see "E-mail вказано невірно" text in email field

  @focus
  Scenario: Email with space in middle
    When I type "regist ration@mailforspam.com" in "email field"
    And I type an unregistered phone number
    And I type "qWERTY1234@" in "password field"
    And I uncheck "I want to get an INTERTOP PLUS card" checkbox
    And I check "I agree with terms" checkbox
    And I click on "Registration" button
    Then I see "E-mail вказано невірно" text in email field

  @focus
  Scenario: Email with coma in end
    When I type "registration@mailforspam.com," in "email field"
    And I type an unregistered phone number
    And I type "qWERTY1234@" in "password field"
    And I uncheck "I want to get an INTERTOP PLUS card" checkbox
    And I check "I agree with terms" checkbox
    And I click on "Registration" button
    Then I see "E-mail вказано невірно" text in email field

  @focus
  Scenario: Email with semicolon in end
    When I type "registration@mailforspam.com;" in "email field"
    And I type an unregistered phone number
    And I type "qWERTY1234@" in "password field"
    And I uncheck "I want to get an INTERTOP PLUS card" checkbox
    And I check "I agree with terms" checkbox
    And I click on "Registration" button
    Then I see "E-mail вказано невірно" text in email field

  @focus
  Scenario: Email with special characters
    When I type "r),(,-,_,+registration@mailforspam.com" in "email field"
    And I type an unregistered phone number
    And I type "qWERTY1234@" in "password field"
    And I uncheck "I want to get an INTERTOP PLUS card" checkbox
    And I check "I agree with terms" checkbox
    And I click on "Registration" button
    Then I see "E-mail вказано невірно" text in email field

  @focus
  Scenario: Email already registered
    When I type "authorization@mailforspam.com" in "email field"
    And I type an unregistered phone number
    And I type "qWERTY1234@" in "password field"
    And I uncheck "I want to get an INTERTOP PLUS card" checkbox
    And I check "I agree with terms" checkbox
    And I click on "Registration" button
    Then I see "E-mail authorization@mailforspam.com вже зареєстрований на сайті" text in email field

  @focus
  Scenario: Phone field empty
    When I type an unregistered email
    And I type "qWERTY1234@" in "password field"
    And I uncheck "I want to get an INTERTOP PLUS card" checkbox
    And I check "I agree with terms" checkbox
    And I click on "Registration" button
    Then I see "Телефон не заповнений" text in phone field

  @focus
  Scenario: Phone incorrect
    When I type an unregistered email
    And I type "000000000" in "phone field"
    And I type "qWERTY1234@" in "password field"
    And I uncheck "I want to get an INTERTOP PLUS card" checkbox
    And I check "I agree with terms" checkbox
    And I click on "Registration" button
    Then I see "Невірний формат телефона" text in phone field

  @focus
  Scenario: Phone incorrect
    When I type an unregistered email
    And I type "+380963570987" in "phone field"
    And I type "qWERTY1234@" in "password field"
    And I uncheck "I want to get an INTERTOP PLUS card" checkbox
    And I check "I agree with terms" checkbox
    And I click on "Registration" button
    Then I see "Телефон +380963570987 вже зареєстрований на сайті" text in phone field

  @focus
  Scenario: Phone less then 10 symbols
    When I type an unregistered email
    And I type "+380963570" in "phone field"
    And I type "qWERTY1234@" in "password field"
    And I uncheck "I want to get an INTERTOP PLUS card" checkbox
    And I check "I agree with terms" checkbox
    And I click on "Registration" button
    Then I see "Невірний формат телефона" text in phone field

  @focus
  Scenario: Password without special characters
    When I type an unregistered email
    And I type an unregistered phone number
    And I type "qWERTY1234" in "password field"
    And I uncheck "I want to get an INTERTOP PLUS card" checkbox
    And I check "I agree with terms" checkbox
    And I click on "Registration" button
    Then I see "Пароль мін 10 символів, 1 спец. символ, 1 цифра, буква в верхньому і нижньому регістрі" text in password field

  @focus
  Scenario: Password without numbers
    When I type an unregistered email
    And I type an unregistered phone number
    And I type "qWERTYQWE@" in "password field"
    And I uncheck "I want to get an INTERTOP PLUS card" checkbox
    And I check "I agree with terms" checkbox
    And I click on "Registration" button
    Then I see "Пароль мін 10 символів, 1 спец. символ, 1 цифра, буква в верхньому і нижньому регістрі" text in password field

  @focus
  Scenario: Password in lowercase
    When I type an unregistered email
    And I type an unregistered phone number
    And I type "qwerty123@" in "password field"
    And I uncheck "I want to get an INTERTOP PLUS card" checkbox
    And I check "I agree with terms" checkbox
    And I click on "Registration" button
    Then I see "Пароль мін 10 символів, 1 спец. символ, 1 цифра, буква в верхньому і нижньому регістрі" text in password field

  @focus
  Scenario: Password in uppercase
    When I type an unregistered email
    And I type an unregistered phone number
    And I type "QWERTY123@" in "password field"
    And I uncheck "I want to get an INTERTOP PLUS card" checkbox
    And I check "I agree with terms" checkbox
    And I click on "Registration" button
    Then I see "Пароль мін 10 символів, 1 спец. символ, 1 цифра, буква в верхньому і нижньому регістрі" text in password field

  @focus
  Scenario: Password with space in frond
    When I type an unregistered email
    And I type an unregistered phone number
    And I type " qWERTY1234@" in "password field"
    And I uncheck "I want to get an INTERTOP PLUS card" checkbox
    And I check "I agree with terms" checkbox
    And I click on "Registration" button
    Then I see "Пароль мін 10 символів, 1 спец. символ, 1 цифра, буква в верхньому і нижньому регістрі" text in password field

  @focus
  Scenario: Password with space in end
    When I type an unregistered email
    And I type an unregistered phone number
    And I type "qWERTY1234@ " in "password field"
    And I uncheck "I want to get an INTERTOP PLUS card" checkbox
    And I check "I agree with terms" checkbox
    And I click on "Registration" button
    Then I see "Пароль мін 10 символів, 1 спец. символ, 1 цифра, буква в верхньому і нижньому регістрі" text in password field

  @focus
  Scenario: Password with space in middle
    When I type an unregistered email
    And I type an unregistered phone number
    And I type "qWERTY 1234@" in "password field"
    And I uncheck "I want to get an INTERTOP PLUS card" checkbox
    And I check "I agree with terms" checkbox
    And I click on "Registration" button
    Then I see "Пароль мін 10 символів, 1 спец. символ, 1 цифра, буква в верхньому і нижньому регістрі" text in password field

  @focus
  Scenario: Password with cyrillic
    When I type an unregistered email
    And I type an unregistered phone number
    And I type "qWERаY1234@" in "password field"
    And I uncheck "I want to get an INTERTOP PLUS card" checkbox
    And I check "I agree with terms" checkbox
    And I click on "Registration" button
    Then I see "Пароль мін 10 символів, 1 спец. символ, 1 цифра, буква в верхньому і нижньому регістрі" text in password field


