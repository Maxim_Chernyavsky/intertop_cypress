import utilsSteps from "../../steps/utilsSteps"
import headerSteps from "../../steps/headerSteps"
import authSteps from "../../steps/authSteps"

describe('Registration', () => {

    //Steps
    utilsSteps.openUrl()
    headerSteps.selectLanguage()
    headerSteps.clickAuthIcon()
    authSteps.clickRegistrationBtn()
    authSteps.setRandomEmail()
    authSteps.setRandomPhone()
    authSteps.setPassword()
    authSteps.uncheckIWantBonusCard()
    authSteps.checkIAgreeWithTerms()
    authSteps.checkGenerateAPassword()
    authSteps.uncheckIAgreeWithTerms()
    authSteps.setEmailWithLength()
    authSteps.setPhone()
    authSteps.setEmail()
    //Expects
    headerSteps.expectSeeText()
    authSteps.expectAlertTextInEmailField()
    authSteps.expectAgreeWithTermsAlertMsgText()
    authSteps.expectAlertTextInPasswordField()
    authSteps.expectAlertTextInPhoneField()

})

