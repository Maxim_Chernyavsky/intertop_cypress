import authPage from "../../pages/AuthPage"
import utils from "../../common/utils"

class AuthSteps {

    setEmail() {
        When('I type {string} in "email field"', (email) => {
            authPage.typeEmail(email)
        });
    }

    setPassword() {
        When('I type {string} in "password field"', (password) => {
            authPage.typePassword(password)
        });
    }

    clickEnterBtn() {
        When('I click on "enter" button', () => {
            authPage.clickLoginBtn()
        });
    }

    logIn() {
        this.setEmail()
        this.setPassword()
        this.clickEnterBtn()
    }

    expectAlertTextInEmailField() {
        Then('I see {string} text in email field', (text) => {
            authPage.getEmailAlertMsgText().should('have.text', text)
        });
    }

    expectAlertTextInPasswordField() {
        Then('I see {string} text in password field', (text) => {
            authPage.getPasswordAlertMsgText().should('have.text', text)
        });
    }

    clickRegistrationBtn() {
        When('I click on "Registration" button', () => {
            authPage.clickRegistrationBtn()
        });
    }

    setRandomEmail() {
        When('I type an unregistered email', () => {
            authPage.typeEmail(utils.randomEmailLength())
        });
    }

    setRandomPhone() {
        When('I type an unregistered phone number', () => {
            authPage.typePhone(utils.randomPhoneLength())
        });
    }

    uncheckIWantBonusCard() {
        When('I uncheck "I want to get an INTERTOP PLUS card" checkbox', () => {
            authPage.checkBonusCardCheckbox(false)
        });
    }

    checkIAgreeWithTerms() {
        When('I check "I agree with terms" checkbox', () => {
            authPage.checkIAgreeCheckbox()
        });
    }

    checkGenerateAPassword() {
        When('I check "Generate a password for me" checkbox', () => {
            authPage.checkGeneratePasswordCheckbox()
        });
    }

    uncheckIAgreeWithTerms() {
        When('I uncheck "I agree with terms" checkbox', () => {
            authPage.checkIAgreeCheckbox(false)
        });
    }

    expectAgreeWithTermsAlertMsgText() {
        Then('I see {string} text in "I agree with terms" field', (text) => {
            authPage.getAgreeWithTermsAlertMsgText().should('have.text', text)
        });
    }

    expectAlertTextInPhoneField() {
        Then('I see {string} text in phone field', (text) => {
            authPage.getPhoneAlertMsgText().should('have.text', text)
        });
    }

    setEmailWithLength() {
        When('I type email with {string} symbols in "email" field', (length) => {
            authPage.typeEmail(utils.randomEmailLength(length))
        });
    }

    setPhone() {
        When('I type {string} in "phone field"', (phone) => {
            authPage.typePhone(phone)
        });
    }

}

module.exports = new AuthSteps()