import {Given} from "cypress-cucumber-preprocessor/steps";
import headerPage from "../../pages/HeaderPage";

class HeaderSteps {

    selectLanguage() {
        Given('I change language on {string}', (lang) => {
            headerPage.clickLanguage()
            headerPage.selectLanguageInDropdown(lang)
        });
    }

    clickAuthIcon() {
        When('I click on auth icon', () => {
            headerPage.clickAuthIcon()
        });
    }

    expectSeeText() {
        Then('I see {string} text in dropdown', (text) => {
            headerPage.getMyProfileText().should('have.text', text)
        });
    }
}

module.exports = new HeaderSteps()