import {Given} from "cypress-cucumber-preprocessor/steps";

class UtilsSteps {

    openUrl() {
        Given('I open {string} page', (string) => {
            cy.clearCookies()
            cy.visit(string)
        });
    }
}

module.exports = new UtilsSteps()